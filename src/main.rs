extern crate clap;
extern crate reqwest;
extern crate serde;

use clap::{App, Arg};
use regex::Regex;
use serde::Deserialize;
use std::env::current_dir;
use std::fs::File;
use std::io::prelude::*;
use std::path::PathBuf;
use std::process::Command;

#[derive(Deserialize, Clone)]
struct Image {
    url: String, // relative URL
    copyright: String,
}

#[derive(Deserialize)]
struct ImageList {
    images: [Image; 1],
}

fn get_image_info() -> Image {
    let list: ImageList = reqwest::get("https://www.bing.com/HPImageArchive.aspx?format=js&n=1")
        .unwrap()
        .json()
        .unwrap();
    list.images[0].clone()
}

fn download_image(relative_url: String) -> Vec<u8> {
    let mut address = String::from("https://bing.com");
    address.push_str(&relative_url);
    let mut res = reqwest::get(&address).unwrap();
    let mut buf: Vec<u8> = vec![];
    res.copy_to(&mut buf).unwrap();
    buf
}

fn save_image(buf: Vec<u8>, filename: &PathBuf) {
    let mut file = File::create(filename).unwrap();
    file.write_all(buf.as_slice()).unwrap();
}

#[cfg(target_os = "linux")]
fn _set(filename: &PathBuf, param: String) {
    Command::new("gsettings")
        .arg("set")
        .arg(param)
        .arg("picture-uri")
        .arg(filename)
        .status()
        .expect("gsettings failed to start");
}

#[cfg(target_os = "linux")]
fn set_wallpaper(filename: &PathBuf) {
    _set(filename, String::from("org.gnome.desktop.background"));
}

#[cfg(target_os = "linux")]
fn set_screensaver(filename: &PathBuf) {
    _set(filename, String::from("org.gnome.desktop.screensaver"));
}

// works after logout & login
#[cfg(target_family = "windows")]
fn set_wallpaper(filename: &PathBuf) {
    Command::new("reg")
        .arg("add")
        .arg(r#"HKCU\control panel\desktop"#)
        .arg("/v")
        .arg("wallpaper")
        .arg("/t")
        .arg("REG_SZ")
        .arg("/d")
        .arg(filename)
        .arg("/f")
        .status()
        .unwrap();
    Command::new("reg")
        .arg("delete")
        .arg(r#"HKCU\Software\Microsoft\Internet Explorer\Desktop\General"#)
        .arg("/v")
        .arg("WallpaperStyle")
        .arg("/f")
        .status()
        .unwrap();
    Command::new("reg")
        .arg("add")
        .arg(r#"HKCU\control panel\desktop"#)
        .arg("/v")
        .arg("WallpaperStyle")
        .arg("/t")
        .arg("REG_SZ")
        .arg("/d")
        .arg("2")
        .arg("/f")
        .status()
        .unwrap();
    Command::new("RUNDLL32.EXE").arg("user32.dll,UpdatePerUserSystemParameters");
    println!("To see new wallpaper logout and the login.");
}

// doesn't work
/*#[cfg(target_family = "windows")]
fn set_screensaver(filename: &PathBuf) {
    Command::new("reg")
        .arg("add")
        .arg(r#"HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\Personalization"#)
        .arg("/v")
        .arg("LockScreenImage")
        .arg("/t")
        .arg("REG_SZ")
        .arg("/d")
        .arg(filename)
        .arg("/f")
        .status()
        .unwrap();
}*/

#[cfg(target_family = "windows")]
fn set_screensaver(_filename: &PathBuf) {
    println!("Setting screensaver on Windows is not supported.");
}

#[cfg(target_os = "macos")]
fn set_wallpaper(filename: &PathBuf) {
    Command::new("osascript")
        .arg("-e")
        .arg(format!(
            r##"tell application "Finder" to set desktop picture to POSIX file "{}""##,
            filename.to_str().unwrap()
        ))
        .status()
        .expect("Setting the wallpaper failed.");
}

#[cfg(target_os = "macos")]
fn set_screensaver(_filename: &PathBuf) {
    println!("Setting screensaver on MacOS is not supported.");
}

fn main() {
    let matches = App::new("Bing Daily Wallpaper utility")
        .arg(Arg::with_name("img-directory"))
        .arg(
            Arg::with_name("desktop-background")
                .short("d")
                .help("set as desktop background"),
        )
        .arg(
            Arg::with_name("screensaver")
                .short("s")
                .help("set as screensaver"),
        )
        .get_matches();
    let path = PathBuf::from(
        matches
            .value_of("img-directory")
            .unwrap_or(current_dir().unwrap().to_str().unwrap())
            .to_string(),
    );

    let image = get_image_info();

    let re = Regex::new(r#"[\\/\?%\*:\|"<>\. ]"#).unwrap();
    let file = re.replace_all(&image.copyright, "_").to_string();

    let filename = path.join(file);

    if !filename.is_file() {
        let buf = download_image(image.url);
        save_image(buf, &filename);
    }

    if matches.is_present("desktop-background") {
        set_wallpaper(&filename);
    }
    if matches.is_present("screensaver") {
        set_screensaver(&filename);
    }
}
